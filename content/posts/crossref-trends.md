---
title: "Crossref Trends"
date: 2021-09-27T12:33:06+02:00
draft: true
author: Geoffrey Bilder
---
# Crossref Trends

## TL;DR

Edward Tufte would not approve- but I don't care.

## How did we do this?

Where did we get the data and what are its limitations?

We had to collect this data from several sources. Eventually we will look to integrate it into the REST API.

The first problem we had is that the REST API doesn't *yet* expose some interesting member demographic information. For example:

- member-type
- the date they joined crossref
- the annual fee level they joined at

So  we have extracted that missing information from the CS system and annotated a copy of the member data that is available from the API.

The second problem with our member data is that we haven't been good about capturing clean, structured, and accurate locaton data about our members.

The scripts in this repo attempt to augment our existing location data using two-passes. The first pass is very naive and just looks for a known country name in the contact infromation string that we do have for a member. The second pass attempts to *resolve* names (using the APIs from  [Open Street Map](https://www.openstreetmap.org/#map=6/46.449/2.210) and [Geonames](https://www.geonames.org/)) to countries by looking up city names, region names, etc. Between these two techniques, we seem to be able to get enhanced geolocation data for all but ~40 of our 17K members.

And the third problem with our member data is that we do not have easy access to temporal data. So, for example, we do not have easy access to how many DOIs (and of what type) were registered by each member for each year. Instead we have publication dates. But what this means is that the growth rates shown are probably a bit artificially smooth. We know, for example, that many members joined and then took several years to register backfiles. But it these charts it looks like a member who has been prublishing since the 18th century suddenly registered *all* of their DOIs when Crossref was founded in 2000.

Again, we'll investigate to see if we can reconstruct this temporal information from another source- like our historical invoicing records.

In the mean time- enjoy the eye candy (or, more probably, [chartjunk](https://en.wikipedia.org/wiki/Chartjunk))



## Members joining Crossref by country

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7145935"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

## DOIs registered by country

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7126154"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

## Members joining Crossref by region

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7146890"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

## DOIs registered by region

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7153027"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

## Members joining crossref by member type

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7151862"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

## DOIs registered with Crossref by member type

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7153074"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

## Members joining Crossref by annual fee

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7151848"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

## DOIs registered with Crossref by member's annual fee

<div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/7151795"><script src="https://public.flourish.studio/resources/embed.js"></script></div>

