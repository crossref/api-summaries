
## Member Publisher

Number: 8078

What we traditonally think of as a "member". They sign-up on their own, they pay a fee, they vote.

## REPRESENTED_MEMBER

Number: 9224

### Examples

- 6661
- 16468
- 16462


### Comments

Represented by who?

They pay no annual fee.

Can they vote?

The following "member" is a "REPRESENTED_MEMBER" according to CS, but they don't have a "join-date"

"Hwarang Dae Research Institute"
https://api.crossref.org/members/15966
https://doi.crossref.org/getPrefixPublisher/?prefix=10.31066

## Sponsored Publisher

Number: 2414

### Examples

- 4903
- 4902
- 24752

### Comments

pay no fee.
deposit dois
vote?

## RA_SPONSORED_MEMBER

Number: 29

### Examples

- 11344
- 16382
- 29254

### Comments

Looks like this is weird thing we did with Medra.
Pay no fee
Deposit DOIs (including current)
Vote?


## AGENT

### Examples

- Atypon
- Metapress
- Allen Press

### Comments

- Looks like they pay fee of 2000.
- Looks like they don't normally deposit DOIs of their own- though Metapress seems to have 108 oprphaned DOIs


## Sponsoring Publisher

Now Known as: Sponsoring member?

Number: 50
Vote:Yes
Fee:Yes

Examples:

- Japan Science and Technology Agency (JST) (272)
- AIP Publishing (317)
- Project Muse (147)
- Organisation for Economic Co-Operation and Development  (OECD) (1963)
- BioOne (404)

