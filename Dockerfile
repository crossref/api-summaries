FROM python:3.9.6-buster

ADD requirements.txt requirements.txt 
#RUN apk update
#RUN apk add libxslt-dev
RUN apt-get update
RUN apt-get upgrade -y
#RUN apt-get install libxml2 libxml2-dev libxslt-dev
RUN apt-get install -y libxml2-dev libxslt-dev python-dev
RUN python -m venv venv
RUN . venv/bin/activate
RUN pip install --upgrade pip
RUN pip install -r requirements.txt







