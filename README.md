# API Summaries

## TL;DR

We have [cool graphs of demographic data about Crossref membership growth and DOI growth](https://crossref.gitlab.io/api-summaries/posts/crossref-trends/).

## Motivation

Can we routinely store Crossref API results as artifacts for reuse in other projects?

Can we annotate these artifacts with extra information gleaned from our internal systems and external APIs with an eye toward including the extra data in the API itself, eventually?

Can we test 'fixes' for bugs in the current API output? 

## Technical

To test locally copy `env.example` to  `.env` file and set the following variables in `.env`:

- CI_PROJECT_TITLE
- GITLAB_USER_EMAIL


The geolocate script does a first pass where it tries to quickly locate anything that is already stored in a caached db called locations.db. As we add/modify members, this database will get out-of-date.

To seed or manually update `locations.db` run:

```
python geolocate.py -u -f <members json file> 
```

## Downloading artifacts. 

To download the latest data you can just:

```
curl -sSL "https://gitlab.com/crossref/api-summaries/-/jobs/artifacts/main/download?job=annotate" -o artifacts.zip
```

## TODO

- add log of gelocation errors to artifacts
- generate summary report
- generate json artifact suitable for populating autocomplete widgets
