# Reports

The csv files in this directory are summaries of the member data gathered from the API. They are used for generating the charts and diagrams linked to from this repo. Use `summary.csv` if you want to sanity-check calculations.

