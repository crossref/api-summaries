import json
import logging
import sys

logger = logging.getLogger(__name__)


def add_openref_info(members):
    """annotate dump of member records to indicate
    if they open references. A temporary kludge while
    the REST API is in code-freeze"""
    results = []
    for member in members:
        logger.info(f"processing member: {member['id']}")
        try:
            if member["flags"]:
                member["flags"]["deposits-some-public-references"] = False
                member["flags"]["deposits-some-closed-visible-references"] = False
                member["flags"]["deposits-some-open-visible-references"] = False
                if member["prefix"]:
                    for prefix in member["prefix"]:
                        if prefix.get("public-references", False):
                            member["flags"]["deposits-some-public-references"] = True
                        if prefix.get("reference-visibility", False) == "closed":
                            member["flags"][
                                "deposits-some-closed-visible-references"
                            ] = True
                        if prefix.get("reference-visibility", False) == "open":
                            member["flags"][
                                "deposits-some-open-visible-references"
                            ] = True
                results.append(member)
            else:
                logger.warning(f"no flags in member record: {member['id']}")
        except Exception as e:
            logger.error(f"broken member record: {member['id']}: {e}")
            continue

    return results


if __name__ == "__main__":
    try:
        member_list_fn = sys.argv[1]
        # destinattion_fn = sys.argv[2]

        with open(member_list_fn) as src:
            member_list = json.load(src)

        # annotated_member_list = annotate(member_list)

        # with open(destinattion_fn,'w') as dest:
        #     dest.write(json.dumps(annotated_member_list,indent=4))
        print(json.dumps(add_openref_info(member_list), indent=4))

    except Exception as e:
        logger.error(
            f"You must provide the members filename and destination filename as arguments: {e}"
        )

        sys.exit(1)
