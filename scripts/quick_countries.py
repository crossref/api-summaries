import logging
import sys
import re
from pprint import pprint
from time import time

import pycountry

logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)


def add_alternatives() -> dict:
    """
    This returns a dict which maps alternative names to the
    pycountry numeric code for the country.

    country names are a political minefield and it seems that
    most of the ones we have to patch this way are places where
    there is some controversy about the status of the location
    """

    alternatives = {
        "South Korea": "Korea, Republic of",
        "Russia": "Russian Federation",
        "Palestine": "the State of Palestine",
        "Taiwan": "Taiwan, Province of China",
        "Vietnam": "Socialist Republic of Viet Nam",
        "Venezuela": "Bolivarian Republic of Venezuela",
        "Bolivia": "Plurinational State of Bolivia",
        "Macedonia": "North Macedonia",
        #'Czech Republic': 'xxx'
    }

    return {
        alternative_name: pycountry.countries.lookup(official_name).numeric
        for alternative_name, official_name in alternatives.items()
    }


def map_country_to_id():
    mapping = {}
    for country in pycountry.countries:
        mapping[country.name] = country.numeric
        try:
            mapping[country.official_name] = country.numeric
        except AttributeError:
            pass
    return mapping


name_to_id = map_country_to_id()
# add/replace some of those provided in pycountry with the alternatives defined above
name_to_id = {**name_to_id, **add_alternatives()}


def quick_country_lookup(text):
    candidates = [
        name for name, _ in name_to_id.items() if re.search(rf"\b{name}\b", text)
    ]
    if candidates:
        if len(candidates) > 1:
            logger.warning(f"{len(candidates)} candidates found for {text}")
        # if there are several candidates, we return the one that occurs the
        # furthest to the right in the string
        # e.g. avoid returning name "Jersey" for a text entry that contains:
        # "New Jersey, United States"
        return sorted(
            candidates,
            key=lambda candidate: re.search(rf"\b{candidate}\b", text).span()[0],
        )[-1]

    return None


if __name__ == "__main__":
    fn = sys.argv[1]
    with open(fn, "r", encoding="UTF-8") as f:
        while line := f.readline().rstrip():
            t0 = time()
            country = quick_country_lookup(line)
            print(f"lookup time: {time() - t0}")
            if country:
                print(f"Success: {line} -> {country}")
            else:
                print(f"Failed: {line}")
            print("-" * 40)
