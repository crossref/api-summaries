import logging
import concurrent.futures
import sys
import os
from turtle import clear
import requests
import json
from diskcache import Cache


from tenacity import retry, stop_after_attempt, wait_random_exponential


example_issn_work_cache = Cache("example_issn_work_cache")
example_issn_work_cache.stats(enable=True)


logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)

THIRTY_DAYS = 30 * 86400

CI_PROJECT_TITLE = os.getenv("CI_PROJECT_TITLE", "Unknown")
CONTACT = os.getenv("GITLAB_USER_EMAIL", "Unknown")
USER_AGENT = f"{CI_PROJECT_TITLE}:{os.path.basename(__file__)} ; (mailto:{CONTACT})"
HEADERS = {"User-Agent": USER_AGENT}


@retry(stop=stop_after_attempt(5), wait=wait_random_exponential(multiplier=1, max=60))
@example_issn_work_cache.memoize()
def get_member_info(issn):
    logger.info(f"Looking up sample work for ISSN: {issn}")
    try:
        uri = f"https://api.crossref.org/journals/{issn}/works?rows=1"
        sample_work = requests.get(uri, headers=HEADERS).json()
        if len(sample_work["message"]["items"]) == 0:
            member = None
            publisher = None
        else:
            member = sample_work["message"]["items"][0]["member"]
            publisher = sample_work["message"]["items"][0]["publisher"]
        return {
            "member": member,
            "publisher": publisher,
        }
    except Exception as e:
        logger.error(
            f"get_member_info failed for: {issn}. possible orphaned journal info."
        )
        return {}
        # raise e from e


def annotate(journal):
    try:
        journal["owner-prefix-name"] = journal["publisher"]
        member_info = get_member_info(journal["ISSN"][0])
        return {**journal, **member_info}
    except Exception as e:
        logger.warning(f"no ISSN for: {journal}")


def main(fn):
    with open(fn) as json_file:
        journals = json.load(json_file)

    journals_with_issns = [journal for journal in journals if len(journal["ISSN"]) > 0]
    logger.info(f"total journals: {len(journals)}")
    logger.info(f"journals with ISSNs: {len(journals_with_issns)}")

    # TODO remove old debugging code?
    short_list = journals_with_issns[:]

    items = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
        futures = {
            executor.submit(annotate, journal): journal for journal in short_list
        }

    for future in concurrent.futures.as_completed(futures):
        try:
            data = future.result()
            items.append(data)
        except Exception as e:
            logger.error(f"-->{e}")
    logger.info(f"annotated {len(items)} journal entries")
    hits, misses = example_issn_work_cache.stats(enable=False, reset=True)
    logger.info(f"cache hits {hits} / cache misses {misses}")
    print(json.dumps(items, indent=4))


if __name__ == "__main__":
    try:
        fn = sys.argv[1]
        main(fn)
    except Exception as e:
        print(f"You must provide a journal list as an argument: {e}")
        raise e from e
