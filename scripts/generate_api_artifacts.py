import logging
import concurrent.futures
import time
import sys
import os
import requests
import json

# from dotenv import load_dotenv
from tenacity import retry, stop_after_attempt, wait_random_exponential

# load_dotenv()

logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)

API_BASE = "https://api.crossref.org"

CI_PROJECT_TITLE = os.getenv("CI_PROJECT_TITLE", "Unknown")
CONTACT = os.getenv("GITLAB_USER_EMAIL", "Unknown")
USER_AGENT = f"{CI_PROJECT_TITLE}:{os.path.basename(__file__)} ; (mailto:{CONTACT})"

HEADERS = {"User-Agent": USER_AGENT}


class APISummariesError(Exception):
    """Exception class from which every exception in this library will derive.
    It enables other projects using this library to catch all errors coming
    from the library with a single "except" statement
    """

    pass


class TenaciousnessCrapiFailed(APISummariesError):
    """Tenaciousness was not enough"""

    pass


@retry(stop=stop_after_attempt(5), wait=wait_random_exponential(multiplier=1, max=60))
def crapi(spec):
    query = spec["query"]
    headers = spec["headers"]

    res = requests.get(query, headers=headers)

    if res.status_code == 200:
        return res.json()["message"]

    logger.warning(f"[{res.status_code}] {query}")
    raise TenaciousnessCrapiFailed(f"An HTTP error occurred: status:{res.status_code}")


def all_results_offset(api=API_BASE, route="members", rows=10, headers=None):
    if headers is None:
        headers = {}
    first_page = crapi({"query": f"{api}/{route}?rows={rows}", "headers": headers})
    total_results = first_page["total-results"]
    # TK
    # rows = min(rows,total_results)
    items = first_page["items"]

    queries = [
        {"query": f"{api}/{route}?rows={rows}&offset={offset}", "headers": headers}
        for offset in range(rows, total_results, rows)
    ]

    with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
        future_to_url = {
            executor.submit(crapi, spec): spec["query"] for spec in queries
        }

        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
                items += data["items"]
            except Exception as exc:
                logger.error("%r generated an exception: %s" % (url, exc))

    return total_results, items


def all_results_cursor(api=API_BASE, route="members", rows=10, headers=None):
    params = {"cursor": "*", "rows": rows}
    items = []
    with requests.Session() as s:
        while True:
            prepared = requests.Request(
                method="GET", url=f"{api}/{route}", params=params, headers=headers
            ).prepare()
            logger.info(prepared.url)
            res = s.send(prepared)
            if res.status_code != 200:
                raise ConnectionError(f"API returned code {res.code}")

            record = res.json()

            # are we done?
            if not record["message"]["items"]:
                return record["message"]["total-results"], items
            # more to go
            items += record["message"]["items"]
            logger.info(
                f"retreived {len(items)} of {record['message']['total-results']}"
            )
            params["cursor"] = record["message"]["next-cursor"]


def save_results(fn, data):
    with open(f"data/{fn}.json", "w") as f:
        json.dump(data, f, indent=4)


def snapshot_routes():
    offset_routes = ["types"]
    cursor_routes = ["members", "journals", "funders"]

    for route in cursor_routes:
        logger.info(f"getting all_results for /{route}")
        _, all_data = all_results_cursor(route=route, rows=1000, headers=HEADERS)
        save_results(route, all_data)
    for route in offset_routes:
        logger.info(f"getting all_results for /{route}")
        _, all_data = all_results_offset(route=route, rows=1000, headers=HEADERS)
        save_results(route, all_data)


if __name__ == "__main__":
    logger.info("starting")
    logger.info(f"User-Agent: {USER_AGENT}")
    t0 = time.time()
    snapshot_routes()
    t1 = time.time() - t0
    logger.info(f"Done (time to generate snapshots: {t1})")
