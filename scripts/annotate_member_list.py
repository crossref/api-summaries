import logging
import concurrent.futures
import time
import sys
import os
import requests
import json
from bs4 import BeautifulSoup

from tenacity import retry, stop_after_attempt, wait_random_exponential


logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)


CI_PROJECT_TITLE = os.getenv("CI_PROJECT_TITLE", "Unknown")
CONTACT = os.getenv("GITLAB_USER_EMAIL", "Unknown")
USER_AGENT = f"{CI_PROJECT_TITLE}:{os.path.basename(__file__)} ; (mailto:{CONTACT})"


class APISummariesError(Exception):
    """Exception class from which every exception in this library will derive.
    It enables other projects using this library to catch all errors coming
    from the library with a single "except" statement
    """

    pass


class TenaciousnessFailed(APISummariesError):
    """Tenaciousness was not enough"""

    pass


HEADERS = {"User-Agent": USER_AGENT}


@retry(stop=stop_after_attempt(5), wait=wait_random_exponential(multiplier=1, max=60))
def tenacious_request(prefix):
    query = f"http://doi.crossref.org/getPrefixPublisher/?prefix={prefix}"

    res = requests.get(query, headers=HEADERS)

    if res.status_code == 200:
        return res.text

    logger.warning(f"[{res.status_code}] {query}")
    raise TenaciousnessFailed(f"An HTTP error occurred: status:{res.status_code}")


def extract_elements(xml):
    soup = BeautifulSoup(xml, "xml")
    # member_info[
    #     "allows-public-access-to-refs"
    # ] = soup.publisher.allows_public_access_to_refs.string
    # member_info["reference-distribution"] = soup.publisher.reference_distribution.string

    return {
        "member-type": soup.publisher.member_type.string,
        "annual-fee": soup.publisher.annual_fee.string,
        "date-joined": soup.publisher.date_joined.string,
    }


def supplementary_data(prefix):
    xml = tenacious_request(prefix)
    return extract_elements(xml)


def annotate(member):
    logger.debug(f"Annotating --> {member['prefixes'][0]}")
    member_info = supplementary_data(member["prefixes"][0])
    return {**member, **member_info}


# def geolocate_all(members):
#     for member in members:
#         member['geonames'] = geolocate.lookup_location(member['location'])
#     return members


def main(fn):
    with open(fn) as json_file:
        members = json.load(json_file)
    # TODO remove old debugging code?
    short_list = members[:]

    items = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        futures = {executor.submit(annotate, member): member for member in short_list}

    for future in concurrent.futures.as_completed(futures):
        try:
            data = future.result()
            items.append(data)
        except Exception as e:
            logger.error(f"{e}")

    # re-enable when debugged
    # items = geolocate_all(items)
    print(json.dumps(items, indent=4))


if __name__ == "__main__":
    try:
        fn = sys.argv[1]
        main(fn)
    except Exception as e:
        print(f"You must provide a memberlist as an argument: {e}")
        sys.exit(1)
