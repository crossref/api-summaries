""" generate reports from annotated memebrs list """

import json
import logging
from collections import OrderedDict
from datetime import datetime

import pandas as pd

logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)

flag_url_template = "https://public.flourish.studio/country-flags/svg/"
registering_members = ["Member Publisher", "REPRESENTED_MEMBER", "Sponsored Publisher"]

# What years do we want to cover
crossref_lifespan = list(range(2000, datetime.today().year + 1))


member_summaries = []

member_counts_by_region = {}
member_growth_by_region = {}

member_counts_by_country_code = {}
member_growth_by_country_code = {}

member_counts_by_annual_fee = {}
member_growth_by_annual_fee = {}

member_counts_by_member_type = {}
member_growth_by_member_type = {}

zoot = {}


def init_years():
    """returns all years with zero count"""
    return {year: 0 for year in crossref_lifespan}


def deep_inc(key, subkey, d):
    """increment subkey if it exists, create it and initialize it to 1 if it doesn't exist"""
    if key in d and subkey in d[key]:
        d[key][subkey] = d[key][subkey] + 1
    else:
        d[key] = init_years()
        d[key][subkey] = 1


def zero_fill_year_counts(years):
    """takes a list of dicts of year counts and makes sure it spans entire lifetime of member and Crossref-
    filling missing years with zero count"""
    return {
        year: years[year] if year in years else 0
        for year in range(min(years.keys()), max(crossref_lifespan) + 1)
    }


def subsumlist(years_list):
    """turn a list of lists with year counts and turn it into a list of dictionaries,
    for use by subsum"""
    return subsum(zero_fill_year_counts({year[0]: year[1] for year in years_list}))


def subsum(years):
    """take a list of dictionaries with absolute count per year and turn it into a list of dictionaries
    with the cumulative counts (i.e. growth)"""
    total = 0
    partial_sums = {
        year: (total := total + count) for year, count in sorted(years.items())
    }
    return partial_sums


def counts_to_growth(d):
    return {key: subsum(count) for key, count in d.items()}


def add_image_column(df, key, loc=1):
    df["image_url"] = df.apply(
        lambda row: f"{flag_url_template}{row[key].lower()}.svg", axis=1
    )
    col = df.pop("image_url")
    df.insert(loc, col.name, col)
    return df


def replace_index(df, key):
    df.reset_index(level=0, inplace=True)
    df.rename(columns={"index": key}, inplace=True)
    return df


def reshape(df, key):
    """pivot dataframe and insert image_url"""
    df = df.fillna(0).transpose()
    df = replace_index(df, key)
    df = add_image_column(df, key)
    return df


def doi_report(df, key):
    report = df.groupby([key])[crossref_lifespan].apply(sum)
    report = replace_index(report, key)
    report.to_csv(f"reports/dois_registered_by_{key}_by_year.csv", index=True)


def member_report(df, key):
    df = counts_to_growth(df)
    df_by_year = reshape(pd.DataFrame(df), key)
    df_by_year.to_csv(f"reports/member_growth_by_{key}_by_year.csv", index=True)


def summarize(fn="data/annotated_members.json"):
    with open(fn, "r") as f:
        members = json.load(f)

    max_members = 100000
    member_count = 0
    for member in members:
        member_count += 1
        if member_count >= max_members:
            continue
        member_type = member["member-type"]
        if member_type not in registering_members:
            continue
        doi_count = member.get("counts", {"total-dois": 0})["total-dois"]
        if doi_count > 0:
            member_id = name = member["id"]
            name = member["primary-name"]
            region = member["geonames"].get("continent-code", "unknown")
            country_code = member["geonames"].get("country-code", "unknown")
            country_name = member["geonames"].get("country-name", "unknown")
            date_joined = member.get("date-joined", "unknown")
            annual_fee = member.get("annual-fee", "unknown")
            member_type = member.get("member-type", "unknown")

            critical = [
                region,
                country_code,
                country_name,
                date_joined,
                annual_fee,
                member_type,
            ]
            if "unknown" in critical:
                logger.warning(f"member {member['id']} missing data: {critical}")
                continue

            try:
                year_joined, _, _ = date_joined.split("-")
            except AttributeError as e:
                logger.warning(f"{member_id} has borked date-joined: {date_joined}  ")
                continue

            try:
                year_joined = int(year_joined)
            except TypeError as e:
                logger.critical(f"cannot convert {year_joined} to int")
                raise e

            # we record DOI breakdowns by publication year- but this can be misleading because publication year != registration year.
            # And so - obviously- we should not register a publication as having been registered in 1778 just because it was published
            # in 1778. The earliest that a publication could have been registered is the year the member joined Crossref. And the
            # earliest a member could have joined Crossref is the year Crossref was founded- 2000. So we need to turn yearly DOI counts
            # into DOI cumulative growth. Then we need to take the subset of growth since the member joined Crossref. Then we need to
            # normalise the range to the current lifespan of Crossref.

            doi_growth = subsumlist(member["breakdowns"]["dois-by-issued-year"])

            # a member might have been publishing since 1778, but Crossref has only existed since 2000, so to represent
            # Crossref registrations accurately, we just want growth since 2000
            doi_growth_since_cr_founding = {
                year: doi_growth.get(year, 0)
                for year in range(2000, (datetime.today().year + 1))
            }

            entry = OrderedDict(
                {
                    "id": member_id,
                    "member_type": member_type,
                    "name": name,
                    "country": country_name,
                    "country_code": country_code,
                    "region": region,
                    "year_joined": year_joined,
                    "annual_fee": annual_fee,
                }
                | doi_growth_since_cr_founding
            )

            member_summaries.append(entry)

    summary_count = 0
    for member in member_summaries:
        summary_count += 1
        deep_inc(member["region"], member["year_joined"], member_counts_by_region)
        deep_inc(
            member["country_code"], member["year_joined"], member_counts_by_country_code
        )
        deep_inc(
            member["annual_fee"], member["year_joined"], member_counts_by_annual_fee
        )
        deep_inc(
            member["member_type"], member["year_joined"], member_counts_by_member_type
        )
    logger.info(f"summary_count: {summary_count}")


summarize()
summary_df = pd.DataFrame.from_dict(member_summaries)
# Let's add a flag image column for chart-junk
summary_df = add_image_column(summary_df, "country_code", 5)
summary_df.to_csv("reports/summary.csv", index=True)


dois_registered_by_country_by_year = summary_df.groupby(
    ["country", "region", "image_url"]
)[crossref_lifespan].apply(sum)
dois_registered_by_country_by_year.to_csv(
    "reports/dois_registered_by_country_by_year.csv", index=True
)

doi_report(summary_df, "region")
doi_report(summary_df, "annual_fee")
doi_report(summary_df, "member_type")


member_report(member_counts_by_country_code, "country_code")
member_report(member_counts_by_region, "region")
member_report(member_counts_by_annual_fee, "annual_fee")
member_report(member_counts_by_member_type, "member_type")
