import argparse
import json
import logging
import sqlite3
import sys
from collections import Counter
from functools import lru_cache
from time import sleep, time

import requests

from quick_countries import quick_country_lookup

from key_converter import to_kebab

logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)

# GEO_KEYS = [
#     'bbox',
#     'continentCode',
#     'countryCode',
#     'countryName',
#     'lat',
#     'license',
#     'lng',
#     'name',
#     'population',
#     'toponymName'
# ]

GEO_KEYS = [
    "bbox",
    "continent-code",
    "country-code",
    "country-name",
    "lat",
    "license",
    "lng",
    "name",
    "population",
    "toponym-name",
]

con = sqlite3.connect("locations.db")

sql_create_locations_table = """ CREATE TABLE IF NOT EXISTS locations (
                                        id integer PRIMARY KEY,
                                        recorded_location varchar NOT NULL UNIQUE,
                                        country varchar NOT NULL,
                                        geodata text NOT NULL
                                        
                                    ); """
sql_create_recorded_location_index = """CREATE UNIQUE INDEX IF NOT EXISTS idx_recorded_location 
ON locations (recorded_location);"""

sql_create_location_record = (
    """INSERT INTO locations(recorded_location,country, geodata) VALUES (?,?,?);"""
)
sql_lookup_location = """SELECT geodata FROM locations WHERE recorded_location = ?"""
sql_count_records = """SELECT count(*) FROM locations"""


cur = con.cursor()
cur.execute(sql_create_locations_table)
cur.execute(sql_create_recorded_location_index)

cur.execute(sql_count_records)
data = cur.fetchone()
logger.info(f"Record count: {data}")


@lru_cache
def geonames_geolocate(country):
    # tantalising, a lot more data (e.g. continents, currency) but API matching sucks.
    logger.info(f"using geonames for {country}")
    url = "http://api.geonames.org/searchJSON"
    params = {
        "q": country,
        "maxRows": 1,
        "lang": "en",
        "style": "full",
        "featureClass": "A",
        "username": "crossref",
    }
    if country == "":
        return None
    try:
        resp = requests.get(url=url, params=params)
        if resp.status_code == 200:
            results = resp.json()
            if results["totalResultsCount"] > 0:
                first = results["geonames"][0]
                first["license"] = {
                    "url": "http://creativecommons.org/licenses/by/4.0/",
                    "org": "Geonames (geonames.org)",
                }
                return results["geonames"][0]
            else:
                return None
        else:
            logger.warning(f"HTTP error: '{country}' ({resp.status_code})")
            return None
    except requests.exceptions.InvalidURL as e:
        logger.error(f"geonames_geolocate: invalid URL for: {country}")
        # something very wrong. bail.
        raise SystemExit(e)
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        raise SystemExit(e)


@lru_cache
def osm_geolocate(location):
    logger.info(f"using osm for {location}")
    url = "https://nominatim.openstreetmap.org/search"
    params = {"q": location, "format": "json", "addressdetails": 1, "extratags": 1}
    headers = {
        "Accept-Language": "en-US,en;q=0.5",
        "User-Agent": "Crossref Member locator; mailto:gbilder@crossref.org",
    }
    if location == "":
        return None
    try:
        resp = requests.get(url=url, params=params, headers=headers)
        logger.info(f"url: {resp.request.url}")

        if resp.status_code == 200:
            results = resp.json()

            if len(results) > 0:
                return results[0]
            else:
                return None
        else:
            logger.warning(f"HTTP error: '{location}' ({resp.status_code})")
            return None
    except requests.exceptions.InvalidURL as e:
        logger.error(f"osm_geolocate: invalid URL for: {location}")
        # something very wrong. bail.
        raise SystemExit(e)
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        raise SystemExit(e)


def geo_summary(geo_record):
    if geo_record == {}:
        return geo_record
    try:
        return {k: geo_record[k] for k in GEO_KEYS}
    except Exception as e:
        logger.error(geo_record)
        raise e


def get_country_details(recorded_location):
    country = None
    country = quick_country_lookup(recorded_location)
    if country:
        logger.info(f"quickmatched: {recorded_location} --> {country}")
        return geonames_geolocate(country)

    # if we don't get a country the quick way, try to use OSM to reolve country
    logger.warning(f"failed to quickmatch: {recorded_location}")
    logger.info(f"attempting to osmmatch: {recorded_location}")
    osm_result = osm_geolocate(recorded_location)
    if osm_result:
        country = osm_result["address"]["country"]
        logger.info(f"osmmatched: {recorded_location} --> {country}")
    if not country:
        logger.warning(f"failed to detect country for: {recorded_location}")
        return {}

    return geonames_geolocate(country)


def lookup_location(recorded_location):
    logger.debug(f"looking up geolocation data for {recorded_location}")
    if recorded_location == "":
        return {}
    cur.execute(sql_lookup_location, (recorded_location,))
    data = cur.fetchone()
    if data is None:
        logger.info(f"no geolocation data for {recorded_location}")
        return {}
    logger.debug(f"found geolocation data for {recorded_location}")
    return json.loads(data[0])


def geolocate_all(members):
    for member in members:
        # member['geonames'] = lookup_location(member['location'])
        member["geonames"] = geo_summary(lookup_location(member["location"]))

    return members


def update_location_db(members):
    max_count = 10000
    count = 1
    all_locations = {
        member["location"] for member in members if member["location"] != ""
    }
    for recorded_location in all_locations:
        logger.info(f"processing {count} of {len(all_locations)}")
        cur.execute(sql_lookup_location, (recorded_location,))
        data = cur.fetchone()
        if data is None:
            logger.info(f"new record for {recorded_location}")
            # geodata = get_country_details(recorded_location)
            # country = geodata.get('countryName','unknown')
            geodata = to_kebab(get_country_details(recorded_location))
            # print(geodata)
            country = geodata.get("country-name", "unknown")

            # #OSM version
            # try:
            #     #country = geodata['address']['country']
            #     # country = geodata.get('display_name',None)
            #     # if not country:
            #     country = geodata['address']['country']
            # except Exception as e:
            #     logger.critical(f"Cannot seem to find country name in geodata results")
            #     country = "unknown"
            #     geodata = {}
            #     #raise SystemExit(e)

            cur.execute(
                sql_create_location_record,
                (recorded_location, country, json.dumps(geodata, sort_keys=True)),
            )
            sleep(1)
        else:
            logger.warning(f"record already exists, skipping")
        count = count + 1
        if count > max_count:
            break

    con.commit()
    con.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="sanity check resource update/transfer files"
    )

    parser.add_argument(
        "-f",
        "--file",
        help="path to members json file",
        type=argparse.FileType("r"),
        required=True,
    )

    parser.add_argument("-u", "--update-db", action="store_true", default=False)

    ARGS = parser.parse_args()

    members = json.load(ARGS.file)

    if ARGS.update_db:
        logger.info("updating db...")
        update_location_db(members)
        logger.info("finished updating db")
    else:
        logger.info("geolocating...")
        geolocated = geolocate_all(members)
        logger.info("finished geolocating")
        logger.info("saving results of geolocating")
        with open("data/annotated_members.json", "w+") as f:
            json.dump(geolocated, f)
        # print(json.dumps(geolocated,indent=4))
        logger.info("finished saving results")
