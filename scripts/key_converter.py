import jq

JQ_DEFS = """
def walk(f):
  . as $in
  | if type == "object" then
      reduce keys[] as $key
        ( {}; . + { ($key):  ($in[$key] | walk(f)) } ) | f
  elif type == "array" then map( walk(f) ) | f
  else f
  end;

def map_keys(mapper):
  walk(
    if type == "object"
    then
      with_entries({
        key: (.key|mapper),
	value
      })
    else .
    end
  );

def snake_to_kebab:
  split("_")
  |map(
    select(. != "")
    | ascii_downcase
  )
  | join("-");

def camel_to_kebab:
  [
    splits("(?=[A-Z][a-z])")
  ]
  |map(
    select(. != "")
    | ascii_downcase
  )
  | join("-");

def to_kebab:
  camel_to_kebab | snake_to_kebab;
map_keys(to_kebab)
"""
PROGRAM = jq.compile(JQ_DEFS)


def to_kebab(d: dict) -> dict:
    return PROGRAM.input(d).all()[0]


if __name__ == "__main__":
    j = [
        {
            "foo_bar": {"last_name": 2, "first_name": "Geoffrey"},
            "DOI": "10.5555/12345678",
            "journalTitle": "The Journal of Foo",
            "VolNum": "1",
        }
    ]
    print(to_kebab(j))
