import argparse
import json
import sqlite3
from pathlib import PurePath

import pandas as pd


def new_format_path(existing_path, new_extension):
    return PurePath(existing_path).with_suffix(new_extension)


def main():
    json_path = ARGS.file.name
    csv_path = new_format_path(json_path, ".csv")
    parquet_path = new_format_path(json_path, ".parquet")
    sqllite_path = new_format_path(json_path, ".db")

    df = pd.json_normalize(
        json.load(ARGS.file),
        sep="-",
    ).fillna(0)
    df.reset_index(drop=True, inplace=True)

    # Save to CSV, then reload into new DF
    df.to_csv(csv_path)
    df = None

    df2 = pd.read_csv(csv_path, low_memory=False)
    # csv adds junk column for index, remove it
    del df2["Unnamed: 0"]

    # Save to parquet
    df2.to_parquet(parquet_path)

    # Save to sqlite
    conn = sqlite3.connect(sqllite_path)
    table_name = str(PurePath(ARGS.file.name).stem)
    df2.to_sql(table_name, con=conn)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="convert a json file into a dataframe and then into csv, parquet and sqlite files"
    )

    parser.add_argument(
        "-f",
        "--file",
        help="path to json file",
        type=argparse.FileType("r"),
        required=True,
    )

    ARGS = parser.parse_args()
    main()
    print("done")
